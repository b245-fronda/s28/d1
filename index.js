// CRUD Operations
	// CRUD operation is the heart of any backend application
	// mastering the CRUD operations is essential for any developer
	// This helps in building character and increasing exposure to logical statements that will help us manipulate
	// mastering the CRUD operations of any languages makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.

// [Section] Create (Inserting document)
	// since MongoDB deals with object as it's structure for documents, we can easily create them by providing objects into our methods.
	// mongoDB shell also uses javascript for it's syntax which makes it convenient for us to understand it's code.

	// Insert One document
		/*
			Syntax:
				db.collectionName.insertOne({object/document});
		*/

	db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "1234567890",
			email: "janedoe@gmail.com"
		},
		courses: ["CSS", "JavaScript", "Python"],
		department: "none"
	});

	// Insert Many
		/*
			Syntax:
				db.collectionName.insertMany([{objectA}, {objectB}]);
		*/

	db.users.insertMany([{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "87654321",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	}, {
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "87654321",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "Sass"],
		department: "none"
	}]);

	db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "1234567890",
			email: "janedoe@gmail.com"
		},
		courses: ["CSS", "JavaScript", "Python"]
	});

	db.users.insertOne({
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "1234567890",
			email: "janedoe@gmail.com"
		},
		courses: ["CSS", "JavaScript", "Python"],
		firstName: "Jane"
	});

	db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "1234567890",
			email: "janedoe@gmail.com"
		},
		courses: ["CSS", "JavaScript", "Python"],
		gender: "Female"
	});

	db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "1234567890",
			phone: "123456789011",
			email: "janedoe@gmail.com"
		},
		courses: ["CSS", "JavaScript", "Python"],
		gender: "Female"
	});

// [Section] Read (find)
	/*
		Syntax:
			// it will show us all the documents in our collection
			db.colllectionName.find();
			// it will show us all the documents that has the given fieldset
			db.colllectionName.find(field: value);
	*/

	db.users.find();

	db.users.find({age: 76});

	db.users.find({firstName: "jane"});

	// finding documents uding multiple fieldsets
		/*
			Syntax:
				db.collectionName.find({fieldA:valueA, fieldB:valueB});
		*/

	db.users.find({lastName: "Armstrong", age: 82});

// [Section] Update (updating documents)
	
	// add document
	db.users.insertOne({
		firstName: "Test",
		lastName: "Test",
		age: 0,
		contact: {
			phone: "00000000",
			email: "test@gmail.com"
		},
		course: [],
		department: "none"
	});

		// updateOne
			/*
				Syntax:
					db.collectionName.updateOne({criteria}, {$set: {field:value}});
			*/

	db.users.updateOne({
		firstName: "Jane"
	}, {
		$set: {
			lastName: "Hawking"
		}
	});

		// updating multiple documents
		/*
			Syntax:
				db.collectionName.updateMany({criteria}, {$set: {
				field: value});
		*/

	db.users.updateMany({firstName: "Jane"}, {
		$set: {
			lastName: "Wick",
			age: 25
		}
	});

	db.users.updateMany({department: "none"}, {
		$set: {
			department: "HR"
		}
	});

	// replace the old document
		/*
			Syntax:
				db.collectionName.replaceOne({criteria}, {document/objecyToReplace});
		*/

	db.users.replaceOne({firstName: "Test"}, {
		firstName: "Chris",
		lastName: "Mortel"
	});

// [Section] Delete (deleting documents)
	
	// Deleting single document
		/*
			Syntax:
				db.collectionName.deleteOne({criteria});
		*/

	db.users.deleteOne({firstName: "Jane"});

	// Deleting multiple documents
		/*
			Syntax:
				db.collectionName.deleteMany({criteria});
		*/

	db.users.deleteMany({firstName: "Jane"});

	// reminder
		// db.collectionName.deleteMany();
		// don't forget to add criteria

// [Section] Advanced Queries
	// embedded - object
	// nested - array

	// query on an embedded document
	db.users.find({"contact.phone": "87654321"});

	// query an array with exact elements
	db.users.find({courses: ["React", "Laravel", "Sass"]});

	// querying an array without regards to order and elements
	db.users.find({courses: {$all: ["React"]}});